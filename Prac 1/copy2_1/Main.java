/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.io.*;
/**
 *
 * @author Ntokozo motsumi
 * MTSTO015 
 * University of Cape Town
 */
public class Main {


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        int level = 0,toStack =0, levChange=0;
        
        // TODO code application logic here
        File file = new File(args[0]);
        BufferedReader br = new BufferedReader(new FileReader(file));
        HashMap<String,String> hm=new HashMap<String,String>(5); 
        HashMap<String,String> overwrite=new HashMap<String,String>(); 
        
        Stack< String> stack = new Stack<>();
        Stack< String> stackScope = new Stack<>();
        String st, key, val,item ="null -1";
        String[] undo, levelCheck ;
        
        while ((st = br.readLine()) != null){
          if (st.equals("beginscope")){
              level++;                  //level for scope accessibility allowance
              levChange =0;
              stackScope.push("null 1");   // identifier for stack to indicate when to stop deleting variable no longer in scope
              System.out.println(st);
          } else if (st.equals("endscope")){
              level--;
              levChange =0;
             while (!(item.equals("null 1"))){   //removing end scope items
                item = stackScope.pop();
                levelCheck = item.split(" ");
                
                if ( Integer.parseInt(levelCheck[1]) >= level){ //remove item in hashmap if out of scope
                  hm.remove(levelCheck[0]); 
                  if (overwrite.containsKey(levelCheck[0])){
                      hm.put(levelCheck[0], overwrite.get(levelCheck[0])); ///------ change here
                      overwrite.remove(levelCheck[0]);
                  }
//                  for (int i = 0; i<toStack ;i++){
//                  undo = stack.pop().split(" ");  //undoing overwritten variables, i.e putting back variables with lifeTime
//                  hm.put(undo[0], undo[1]);   
//                    }
//                    toStack=0;
                }
                
              }
                
              item = "null -1";              //resetting to default
              
             
              System.out.println(st);
          }
          String[] splitS = st.split(" ");
          if (splitS[0].equals("define")){  //define operarion for adding variable
              
              key = splitS[1];
              val = splitS[2];
            
              if (hm.containsKey(splitS[1])){
                  int tempLevel = Integer.parseInt(hm.get(splitS[1]).split(" ")[1]);
                  //toStack++;                              //counter to keep track of stacked item, i.e variables with lifetime but are overwritten by variables in current scope
                  //stack.push(key +" "+ hm.get(splitS[1]));     //adding item to stack if already present in hashtable
                  if (tempLevel != level){
                  overwrite.put(key,hm.get(splitS[1])); //----//chnage here also
                  }
              }
              
              hm.put(key , val +" "+ level  );
              levChange =1;
              stackScope.push(key + " " + level);  //for later operation with removing out of scope variables
              System.out.println(st);
          } else if (splitS[0].equals("use")){ //for using variables
             if (hm.containsKey(splitS[1])) {
               System.out.println("use "+ splitS[1]+" = "+ hm.get(splitS[1]).split(" ")[0]); ///-----/// change here            
             } else{
              System.out.println("use "+ splitS[1]+" = undefined"); 
             }
          }
         
        }
       // System.out.println(level);

    }

    
}
