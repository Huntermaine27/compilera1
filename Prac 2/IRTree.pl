use diagnostics;
use feature 'say';

use feature "switch" ;
use feature 'state';


# creatinf array $ARGV[0]
my @arr = getIRTree('testdata7.ir');
loopFold(@arr);
#printIRTree(constFold(@arr));
sub getIRTree{
    my ($fileName) =@_;
    my @array=();
    open(my $fh, '<', $fileName)
        or die "Failed to open file: $!\n";
    while(<$fh>) {
        chomp;
        #say  $_.((rindex $_, "=") +1);
        push @array, $_." ".((rindex $_, "=") +1);
    }
    close $fh;
    return @array;
}

sub refreshIRTree{
    my @array = @_;
    my $len = scalar @array;
    my @retArr =();
    for ( my $i=0; $i<$len; $i++ ){
        my $item =$array[$i];
        my @temp = split / /, $array[$i];
        #chop $item;
        push @retArr, $temp[0];
        }
   return @retArr;
}

sub loopFold {
    my (@array) = @_;
  #  @array = refreshIRTree(@array);
  my  @arrayBody =();
   my ($lBound,$uBound) =(0,0);
   my($maxLevel,$maxIndex,$counter) =(0,0,0);
  my ($label1,$label2,$void) =("","","");
   my ($item,$level)=("",0);
    my $len = scalar @array;
    for ( my $i=0; $i<$len; $i++ ){
        ($item,$level) = splitFunc($array[$i]);
        $item =~ s/=//go;
        if (( (index $array[$i], "CONST") !=-1) && ( (index $array[$i+5], "CONST") !=-1) ){
          if ($level>$maxLevel){
            $maxLevel= $level;
            $maxIndex = $i;
            ($lBound,$void)=$array[$i+1];
            ($uBound,$void)=$array[$i+5];
            say "bounds ", $array[$i+1] ,"\n";
          }
				}
      if ((index $array[$i], "CJUMP") !=-1){
        ($label1,$void) = splitFunc($array[$i+7]);
        ($label2,$void) = splitFunc($array[$i+9]);
        $label1 =~ s/=//go;
        $label2 =~ s/=//go;
      #  say $label1,"\n";
      #  say $label2,"\n";
      }

      if ($item eq $label2) {
        my $previous = $array[$i-1];
        $previous =~ s/=//go;

        #say $previous,"\n";
        if ((index $previous, "LABEL") !=-1){
          ($void, $level) = splitFunc($array[$i+1]);
          #say $item,"\n";
          #say $level;
          my $tempLev =100;
          #say $level;
          while ($tempLev>=$level){
            $counter++;
            ($void, $tempLev) =  splitFunc($array[$i+$counter]);
            #say  $tempLev;
          }
         #say   --$counter;
         @arrayBody = splice @array ,$i+1, $counter-1;
        }
      }

        }
        printIRTree(@arrayBody);
    }

sub splitFunc {
  my ($inputt) = @_;
  my @temp = split / /, $inputt;
  return ($temp[0],$temp[1]);

}

sub printIRTree{
    my @array = @_;
    for (@array){
      say $_;
      }

}
