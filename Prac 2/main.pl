#!/usr/bin/perluse warnings;
use diagnostics;

use feature 'say';

use feature "switch" ;
use feature 'state';
# creatinf array $ARGV[0]
my @arr = getIRTree($ARGV[0]);

printIRTree(constFold(@arr));
sub getIRTree{
    my ($fileName) =@_;    
    my @array=();
    open(my $fh, '<', $fileName)
        or die "Failed to open file: $!\n";
    while(<$fh>) { 
        chomp; 
        #say  $_.((rindex $_, "=") +1);
        push @array, $_." ".((rindex $_, "=") +1);
    } 
    close $fh;
    return @array;
}

sub constFold{
    
    my @array= @_;
    my $bool = 0;
    my (@Indexes,@level,@operator) = ((),(),());
    while ($bool !=1){
    $bool=1;
    my $len = scalar @array;
    my $lastEq= 0;
    for ( my $i=2; $i<$len; $i++ ){
       #say $array[$i]; 
       my @temp = split / /, $array[$i];
       my @temp2 = split / /, $array[$i-2];
       my $levFirst=$temp[1] ;
      # say  $temp[1];
       my $levSecond=$temp2[1];
       if (( (index $array[$i], "CONST") !=-1) && ( (index $array[$i-2], "CONST") !=-1) ){
		if ($levFirst == $levSecond){
		    $lastEq =(rindex $array[$i-3], "=")+1;
		    $bool =0;
		    push @Indexes, $i;
		    push @level, $levFirst;
		    push @operator, substr( $array[$i-3], $lastEq,1);
		   # say   substr( $array[$i-3], $lastEq,1);
		}
          }
     
    }
    if($bool==0){
    @array = refreshIRTree(@array);
    $len = scalar @Indexes;
    for ( my $j=0; $j<$len; $j++ ){
        my $pos = pop @Indexes;
        my $lev = (pop @level)-1;
        my $op = pop @operator;
        my $num1= $array[$pos+1];
        my $num2 = $array[$pos-1];
        $num1 =~ s/=//go;
        $num2 =~ s/=//go;
        #chop $num1;
        #chop $num2;
        my $val =0;
        if ($op eq '+'){
         $val= $num2+$num1;  
        } elsif ($op eq '/'){
        $val= $num2/$num1;
        }elsif ($op eq '*'){
        $val= $num2*$num1;
        } elsif ($op eq '-'){
        $val= $num2-$num1;
        }   
        #say $val  ;
        splice @array, $pos-3, 5;
        $insert1 = "=" x $lev;
        push @array, $insert1."CONST";
        $insert1 = "=".$insert1;
        push @array, $insert1.$val;
        
       # print join(", ", @array), "\n"
    }
     @array = reLevelTree(@array);
    } 
   
    }
  @array = refreshIRTree(@array);  
  return @array;   }

sub refreshIRTree{
    my @array = @_;
    my $len = scalar @array;
    my @retArr =();
    for ( my $i=0; $i<$len; $i++ ){
        my $item =$array[$i];
        my @temp = split / /, $array[$i];
        #chop $item;
        push @retArr, $temp[0];
        }
   return @retArr;
}

sub reLevelTree{
    my @array = @_;
    my $len = scalar @array;
    my @retArr =();
    for ( my $i=0; $i<$len; $i++ ){
        my $item =$array[$i];
       # chop $item;
        push @retArr, $item." ".((rindex $item, "=") +1);
        }
      return @retArr;  
        }
sub printIRTree{
    my @array = @_;    for (@array){
      say $_;
      }
}

